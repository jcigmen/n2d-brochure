package com.noobs2d.brochure.viewpager;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Adapter that describes the ViewPager; number of pages and getters (protected) and setters for the
 * collection of items (Views). Classes that extend this must override
 * {@link FragmentStatePagerAdapter#getItem(int)}
 * 
 * @author MrUseL3tter
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<ViewPagerFragment> viewPageFragments;

    public ViewPagerAdapter(ArrayList<ViewPagerFragment> viewPageFragments, FragmentManager fragmentManager) {
	super(fragmentManager);
	this.viewPageFragments = viewPageFragments;
    }

    public ViewPagerAdapter(FragmentManager fragmentManager) {
	super(fragmentManager);
	viewPageFragments = new ArrayList<ViewPagerFragment>();
    }

    public void addFragment(ViewPagerFragment fragment) {
	viewPageFragments.add(fragment);
    }

    @Override
    public int getCount() {
	return viewPageFragments.size();
    }

    @Override
    public Fragment getItem(int position) {
	throw new RuntimeException("Must override getItem(int) from ViewPagerAdapter!");
    }

    protected ArrayList<ViewPagerFragment> getViewPageFragments() {
	return viewPageFragments;
    }

}
