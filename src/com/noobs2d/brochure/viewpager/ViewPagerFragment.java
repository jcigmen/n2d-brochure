package com.noobs2d.brochure.viewpager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * This class returns a layout created over the design view. Classes that extend this must override
 * {@link Fragment#onCreateView(LayoutInflater, ViewGroup, Bundle)}
 * 
 * @author MrUseL3tter
 */
public class ViewPagerFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	throw new RuntimeException("Must override onCreateView(LayoutInflater, ViewGroup, Bundle) from ViewPageFragment!");
    }

}
