package com.noobs2d.brochure;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.noobs2d.brochure.viewpager.ViewPagerAdapter;
import com.noobs2d.brochure.viewpager.ViewPagerFragment;

public class TestViewPagerAdapter extends ViewPagerAdapter {

    public TestViewPagerAdapter(ArrayList<ViewPagerFragment> viewPageFragments, FragmentManager fragmentManager) {
	super(viewPageFragments, fragmentManager);
    }

    public TestViewPagerAdapter(FragmentManager fragmentManager) {
	super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
	return getViewPageFragments().get(position);
    }
}
