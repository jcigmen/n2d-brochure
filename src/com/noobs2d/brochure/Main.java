package com.noobs2d.brochure;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.noobs2d.brochure.viewpager.ViewPagerAdapter;

public class Main extends FragmentActivity {

    private ViewPager viewPager;

    @Override
    public void onBackPressed() {
	if (viewPager.getCurrentItem() == 0)
	    super.onBackPressed();
	else
	    viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.viewpager_layout);

	viewPager = (ViewPager) findViewById(R.id.pager);
	ViewPagerAdapter adapter = new TestViewPagerAdapter(getSupportFragmentManager());
	adapter.addFragment(new TestViewPagerFragment1());
	adapter.addFragment(new TestViewPagerFragment2());
	adapter.addFragment(new TestViewPagerFragment1());
	adapter.addFragment(new TestViewPagerFragment2());
	viewPager.setAdapter(adapter);
    }

}
