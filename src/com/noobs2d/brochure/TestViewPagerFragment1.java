package com.noobs2d.brochure;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.noobs2d.brochure.viewpager.ViewPagerFragment;

public class TestViewPagerFragment1 extends ViewPagerFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.test_view1, container, false);
	return rootView;
    }

}
